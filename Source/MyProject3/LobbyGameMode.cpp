// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyGameMode.h"
#include "PuzzlePlatformGameInstance.h"





void ALobbyGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	FTimerHandle JoinTimeHandler;
	UWorld* World = GetWorld();
	if (!ensure(World))return;
	GameInstance = Cast<UPuzzlePlatformGameInstance>(GetGameInstance());

	MaxPlayers = GameInstance->GetMaxPlayers();

	if (!ensure(GameInstance))return;

	PlayerCount++;

	if (PlayerCount >= 2)
	{
		World->GetTimerManager().SetTimer(JoinTimeHandler, this, &ALobbyGameMode::JoinLobby, 10.0f);
	}
	
}

void ALobbyGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);
	PlayerCount--;
}

void ALobbyGameMode::JoinLobby()
{
	if (!ensure(GameInstance))return;
	GameInstance->StartSession();
	bUseSeamlessTravel = true;
	GetWorld()->ServerTravel("/Game/Maps/ThirdPersonExampleMap?listen");
}


