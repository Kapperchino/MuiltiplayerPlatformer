// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyProject3GameMode.h"
#include "LobbyGameMode.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT3_API ALobbyGameMode : public AMyProject3GameMode
{
	GENERATED_BODY()

	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;

	void JoinLobby();

	int32 PlayerCount = 0;
	int32 MaxPlayers = 0;
	class UPuzzlePlatformGameInstance* GameInstance = nullptr;
	
};
