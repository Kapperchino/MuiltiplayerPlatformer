// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenu.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "MenuInterface.h"
#include "Components/EditableTextBox.h"
#include "Kismet/KismetSystemLibrary.h"
#include "ServerRow.h"
#include "Components/TextBlock.h"





bool UMainMenu::Initialize()
{
	bool Sucess = Super::Initialize();
	if (!Sucess)return false;

	if (!ensure(HostButton) || !ensure(JoinButton)||!ensure(BackButton)||!ensure(ServerRowClass)||
		!ensure(HostButton2)||!ensure(BackButton2)||!ensure(SessionName))return false;

	HostButton->OnClicked.AddDynamic(this, &UMainMenu::OpenHostMenu);
	HostButton2->OnClicked.AddDynamic(this, &UMainMenu::HostServer);
	JoinButton->OnClicked.AddDynamic(this, &UMainMenu::OpenJoinMenu);
	BackButton->OnClicked.AddDynamic(this, &UMainMenu::BackToMenu);
	BackButton2->OnClicked.AddDynamic(this, &UMainMenu::BackToMenu);
	JoinServerButton->OnClicked.AddDynamic(this, &UMainMenu::JoinServer);
	ExitButton->OnClicked.AddDynamic(this, &UMainMenu::ExitGame);
	return true;

	
	
}

void UMainMenu::SetServerList(TArray<FServerData> ServerNames)
{
	if (!ensure(MenuInterface))return;
	UWorld* World = GetWorld();
	if (!ensure(World))return;

	ServerList->ClearChildren();
	FString Fraction = "";
	for (int32 x = 0; x < ServerNames.Num(); x++)
	{
		UServerRow* Row = CreateWidget<UServerRow>(World, ServerRowClass);
		if (!ensure(Row))return;
		Row->ServerName->SetText(FText::FromString(ServerNames[x].Name));
		Row->HostUser->SetText(FText::FromString(ServerNames[x].HostUsername));
		Fraction = FString::Printf(TEXT("%d/%d"), ServerNames[x].CurrentPlayers, ServerNames[x].MaxPlayers);
		Row->ConnectionFraction->SetText(FText::FromString(Fraction));
		Row->SetUp(this, x);
		ServerList->AddChild(Row);
		
	}
	
}

void UMainMenu::HostServer()
{
	if (MenuInterface)
	{
		FString ServerName = SessionName->Text.ToString();
		MenuInterface->HostServer(ServerName);
	}


}

void UMainMenu::JoinServer()
{
	if (SelectedIndex.IsSet() && ensure(MenuInterface))
	{
		UE_LOG(LogTemp, Warning, TEXT("Selected index %d."), SelectedIndex.GetValue());
		MenuInterface->JoinServer(SelectedIndex.GetValue());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("index not set"));
	}
}

void UMainMenu::ExitGame()
{
	UWorld* World = GetWorld();
	if (!ensure(World))return;
	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (!ensure(PlayerController))return;
	UKismetSystemLibrary::QuitGame(World, PlayerController, EQuitPreference::Quit);
}

void UMainMenu::OpenHostMenu()
{
	if (!ensure(MenuSwitcher) || !ensure(HostMenu))return;
	MenuSwitcher->SetActiveWidget(HostMenu);
}

void UMainMenu::SelectIndex(uint32 Index)
{
	SelectedIndex = Index;
	UpdateChildren();
}



TOptional<uint32> UMainMenu::GetSelectedIndex()
{
	if (SelectedIndex)
		return SelectedIndex;
	else
	{
		return NULL;
	}
}

void UMainMenu::UpdateChildren()
{
	for (int x = 0; x < ServerList->GetChildrenCount(); x++)
	{
		UServerRow* Row = Cast<UServerRow>(ServerList->GetChildAt(x));
		if (Row)
		{
			Row->bIsSelected = (SelectedIndex.IsSet() && SelectedIndex.GetValue()==x);
		}
		
	}
}

void UMainMenu::OpenJoinMenu()
{
	if (!ensure(MenuSwitcher)||!ensure(JoinMenu))return;
	MenuSwitcher->SetActiveWidget(JoinMenu);
	if (MenuInterface)
	{
		MenuInterface->RequestRefresh();
	}
}

void UMainMenu::BackToMenu()
{
	if (!ensure(MenuSwitcher) || !ensure(MainMenu))return;
	MenuSwitcher->SetActiveWidget(MainMenu);
}


