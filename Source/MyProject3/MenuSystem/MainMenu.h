// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuSystem/MenuWidget.h"
#include "MainMenu.generated.h"

/**
 * 
 */

USTRUCT()
struct FServerData
{
	GENERATED_BODY()

	FString Name;
	uint16 CurrentPlayers;
	uint16 MaxPlayers;
	FString HostUsername;
};
UCLASS()
class MYPROJECT3_API UMainMenu : public UMenuWidget
{
	GENERATED_BODY()

	
public:
	

	virtual bool Initialize() override;

	void SetServerList(TArray<FServerData> ServerNames);

	UFUNCTION()
		void HostServer();

	UFUNCTION()
		void OpenJoinMenu();

	UFUNCTION()
		void BackToMenu();

	UFUNCTION()
		void JoinServer();

	UFUNCTION()
		void ExitGame();

	UFUNCTION()
		void OpenHostMenu();

	void SelectIndex(uint32 Index);
	TOptional<uint32> GetSelectedIndex();
	

protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* HostButton = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* JoinButton= nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* JoinServerButton = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* ExitButton = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* BackButton = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* BackButton2 = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* HostButton2 = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UEditableTextBox* SessionName = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UWidgetSwitcher* MenuSwitcher = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UWidget* JoinMenu = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UWidget* MainMenu = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UWidget* HostMenu = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UPanelWidget* ServerList = nullptr;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UUserWidget> ServerRowClass;

	TOptional<uint32> SelectedIndex;
	
	void UpdateChildren();
	
	
	
};
