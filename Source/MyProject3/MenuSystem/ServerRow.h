// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ServerRow.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT3_API UServerRow : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* ServerName = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* HostUser = nullptr;
	
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* ConnectionFraction = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* RowButton = nullptr;

	UPROPERTY()
	class UMainMenu* Parent = nullptr;

	UPROPERTY(BlueprintReadOnly)
		bool bIsSelected;

	void SetUp(class UMainMenu* Parent, int32 index);

	UFUNCTION()
	void OnClicked();


	int32 Index;


	
	
};
