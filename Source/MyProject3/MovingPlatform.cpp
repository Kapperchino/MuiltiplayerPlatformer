// Fill out your copyright notice in the Description page of Project Settings.

#include "MovingPlatform.h"



AMovingPlatform::AMovingPlatform()
{
	PrimaryActorTick.bCanEverTick = true;
	SetMobility(EComponentMobility::Movable);
}

void AMovingPlatform::BeginPlay()
{
	Super::BeginPlay();
	StartLocation = GetActorLocation();
	GlobalTargetLocation = GetTransform().TransformPosition(TargetLocation);
	if (HasAuthority())
	{
		SetReplicates(true);
		SetReplicateMovement(true);
	}
}


void AMovingPlatform::AddActiveTrigger()
{
	ActiveTriggers++;
}

void AMovingPlatform::RemoveActiveTrigger()
{
	ActiveTriggers--;
	FMath::Clamp(ActiveTriggers, 0, 100);
}

void AMovingPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (!HasAuthority())
	{
		return;
	}
	if (ActiveTriggers > 0)
	{
		FVector Location = GetActorLocation();

		if (FVector::Distance(Location, StartLocation) >= FVector::Distance(StartLocation, GlobalTargetLocation))
		{
			FVector Swap = StartLocation;
			StartLocation = GlobalTargetLocation;
			GlobalTargetLocation = Swap;

		}

		Location += (GlobalTargetLocation - StartLocation).GetSafeNormal()*Speed*DeltaTime;
		SetActorLocation(Location);
	}
	
	
	
}


