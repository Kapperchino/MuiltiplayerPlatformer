// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "MovingPlatform.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT3_API AMovingPlatform : public AStaticMeshActor
{
	GENERATED_BODY()
	
public:
	AMovingPlatform();

	virtual void Tick(float DeltaTime)override;
	virtual void BeginPlay()override;

	void AddActiveTrigger();
	void RemoveActiveTrigger();

protected:

	UPROPERTY(EditAnywhere)
		float Speed = 10;

	UPROPERTY(EditAnywhere, Meta = (MakeEditWidget = true))
		FVector TargetLocation;

	UPROPERTY(EditAnywhere)
		int ActiveTriggers = 1;

	FVector StartLocation;
	FVector GlobalTargetLocation;

	

	
	
	
};
