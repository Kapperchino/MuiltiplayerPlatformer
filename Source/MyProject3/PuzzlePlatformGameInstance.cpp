// Fill out your copyright notice in the Description page of Project Settings.

#include "PuzzlePlatformGameInstance.h"
#include "PlatformTrigger.h"
#include "Blueprint/UserWidget.h"
#include "MenuSystem/MainMenu.h"
#include "MenuSystem/InGameMenu.h"
#include "Engine/Engine.h"
#include "OnlineSessionSettings.h"


const FName DESERIED_SERVER_NAME = "TestServer";

void UPuzzlePlatformGameInstance::Init()
{
	Super::Init();
	if (!ensure(Trigger) || !ensure(MenuClass) || !ensure(PauseMenuClass))return;

	OnlineSubSystem = IOnlineSubsystem::Get();
	if (OnlineSubSystem)
	{
		UE_LOG(LogTemp, Warning, TEXT("Found Subsystem: %s"),*OnlineSubSystem->GetSubsystemName().ToString());
		SessionInterface = OnlineSubSystem->GetSessionInterface();
		if (SessionInterface.IsValid())
		{
			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UPuzzlePlatformGameInstance::OnCreateSessionComplete);
			SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UPuzzlePlatformGameInstance::OnDestroySessionComplete);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UPuzzlePlatformGameInstance::OnFindSessionComplete);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UPuzzlePlatformGameInstance::OnJoinSessionComplete);
			
		}

	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("we don't lol"));
	}
}

UPuzzlePlatformGameInstance::UPuzzlePlatformGameInstance(const FObjectInitializer& ObjectInitializer)
{
	

}



void UPuzzlePlatformGameInstance::HostServer(FString ServerName)
{
	if (!SessionInterface.IsValid())
		return;
	DeseriedServerName = ServerName;
	auto CurrentSession = SessionInterface->GetNamedSession(CurrentSessionName);
	
	if (!CurrentSession)
	{
		CreateSession(0, CurrentSessionName);
	}
	else
	{
		SessionInterface->DestroySession(CurrentSessionName);
	}
	
		
}

void UPuzzlePlatformGameInstance::LoadMenuWidget()
{
	if (!ensure(MenuClass))return;
	Menu = CreateWidget<UMainMenu>(this, MenuClass);
	
	Menu->Setup();
	Menu->SetMenuInterface(this);
}

void UPuzzlePlatformGameInstance::PauseGame()
{
	if (!ensure(PauseMenuClass))return;
	UMenuWidget* PauseMenu = CreateWidget<UMenuWidget>(this, PauseMenuClass);
	if (!ensure(PauseMenu))return;

	PauseMenu->Setup();
	PauseMenu->SetMenuInterface(this);
}

void UPuzzlePlatformGameInstance::QuitToMenu()
{
	APlayerController* PlayerController = GetFirstLocalPlayerController(GetWorld());
	if (!ensure(PlayerController))return;

	PlayerController->ClientTravel("/Game/Maps/Menu", ETravelType::TRAVEL_Absolute);
}

void UPuzzlePlatformGameInstance::OnCreateSessionComplete(FName SessionName, bool Success)
{
	if (!Success)
	{
		return;
	}
	if (Menu)
	{
		Menu->TearDown();
	}


	UWorld* World = GetWorld();
	if (!ensure(World))return;
	World->ServerTravel("/Game/Maps/Lobby?listen");
}

void UPuzzlePlatformGameInstance::OnDestroySessionComplete(FName SessionName, bool Sucess)
{
	if (!Sucess)
		return;
	CreateSession(0, CurrentSessionName);
}

void UPuzzlePlatformGameInstance::OnFindSessionComplete(bool Sucess)
{
	if (!Sucess||!SessionSearch.IsValid()||!Menu)
		return;
	TArray<FOnlineSessionSearchResult> Results = SessionSearch->SearchResults;
	TArray<FServerData> ServerNames;

	UE_LOG(LogTemp, Warning, TEXT("Found sessions:"));
	for (int32 x=0;x<Results.Num();x++)
	{
		FServerData Data;
		Data.Name = Results[x].GetSessionIdStr();
		Data.MaxPlayers = Results[x].Session.SessionSettings.NumPublicConnections;
		Data.CurrentPlayers = Data.MaxPlayers - Results[x].Session.NumOpenPublicConnections;
		Data.HostUsername = Results[x].Session.OwningUserName;
		FString ServerName;

		bool bFoundSettings = Results[x].Session.SessionSettings.Get(DESERIED_SERVER_NAME, ServerName);

		if (bFoundSettings)
		{
			Data.Name = ServerName;
		}
		else
		{
			Data.Name = "ERROR CAN NOT FIND NAME";
		}
		ServerNames.Add(Data);
	}

	Menu->SetServerList(ServerNames);
	
}



void UPuzzlePlatformGameInstance::RequestRefresh()
{
	SessionSearch = MakeShareable(new FOnlineSessionSearch());
	if (!SessionSearch.IsValid())
		return;
	SessionSearch->MaxSearchResults = 1000;
	SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
	SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());

}


void UPuzzlePlatformGameInstance::CreateSession(int32 PlayerId,FName SessionName)
{
	if (!SessionInterface.IsValid())
		return;
	FOnlineSessionSettings OnlineSettings;
	if (IOnlineSubsystem::Get()->GetSubsystemName() == "NULL")
	{
		OnlineSettings.bIsLANMatch = true;
	}
	else
	{
		OnlineSettings.bIsLANMatch = false;
	}
	OnlineSettings.NumPublicConnections = MaxPlayers;
	OnlineSettings.bShouldAdvertise = true;
	OnlineSettings.bUsesPresence = true;
	OnlineSettings.Set(DESERIED_SERVER_NAME,DeseriedServerName,EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);
	SessionInterface->CreateSession(0, CurrentSessionName, OnlineSettings);
	
}

void UPuzzlePlatformGameInstance::StartSession()
{
	if (!SessionInterface.IsValid())return;

	SessionInterface->StartSession(CurrentSessionName);

}

void UPuzzlePlatformGameInstance::OnJoinSessionComplete(FName name, EOnJoinSessionCompleteResult::Type)
{
	if (!SessionInterface.IsValid())return;
	FString Address;
	bool bHasConnection = SessionInterface->GetResolvedConnectString(name,Address);
	if (!bHasConnection)
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not get connect string."));
		return;
	}

	UEngine* Engine = GetEngine();
	if (!ensure(Engine))return;
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController))return;

	PlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
	UE_LOG(LogTemp, Warning, TEXT("%s"),*Address);
	
}

int32 UPuzzlePlatformGameInstance::GetMaxPlayers()
{
	return MaxPlayers;
}


void UPuzzlePlatformGameInstance::JoinServer(uint32 Index)
{
	if (!SessionInterface.IsValid()||!SessionSearch.IsValid())return;
	if (Menu)
	{
		Menu->TearDown();
	}

	SessionInterface->JoinSession(0, CurrentSessionName, SessionSearch->SearchResults[Index]);



}



