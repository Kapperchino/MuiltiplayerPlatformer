// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MenuSystem/MenuInterface.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionInterface.h"
#include "PuzzlePlatformGameInstance.generated.h"

/**
 * 
 */



UCLASS()
class MYPROJECT3_API UPuzzlePlatformGameInstance : public UGameInstance, public IMenuInterface
{
	GENERATED_BODY()

		virtual void Init()override;
	
public:

	UPuzzlePlatformGameInstance(const FObjectInitializer& ObjectInitializer);

	

	UFUNCTION(Exec)
		void HostServer(FString ServerName);

	UFUNCTION(Exec)
		void JoinServer(uint32 Index);

	void RequestRefresh();

	UFUNCTION(BlueprintCallable)
		void LoadMenuWidget();

	UFUNCTION(BlueprintCallable)
		void PauseGame();

	UFUNCTION(BlueprintCallable)
		void QuitToMenu();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class APlatformTrigger> Trigger;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class UUserWidget> MenuClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class UUserWidget> PauseMenuClass;

	int32 GetMaxPlayers();

	void StartSession();
	

protected:
	class UMainMenu* Menu = nullptr;
	class UInGameMenu* PauseMenu = nullptr;

	class IOnlineSubsystem* OnlineSubSystem = nullptr;

	IOnlineSessionPtr SessionInterface;

	UPROPERTY(EditDefaultsOnly)
	int32 MaxPlayers = 5;

	FName CurrentSessionName = "Game";

	void OnCreateSessionComplete(FName SessionName,bool Success);

	void OnDestroySessionComplete(FName SessionName, bool Sucess);

	void OnFindSessionComplete(bool Sucess);

	void OnJoinSessionComplete(FName name, EOnJoinSessionCompleteResult::Type);
	
	void CreateSession(int32 PlayerId,FName SessionName);


	
	TSharedPtr<class FOnlineSessionSearch> SessionSearch;

	FString DeseriedServerName;
	
};
